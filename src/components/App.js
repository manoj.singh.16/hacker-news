import React, { Component } from "react";
import Header from "./Header";
import Results from "./Results";
import server from "../api/server";
import Loader from "./Loader";
import Error from "./Error";

// status 0 - loading
// status 1 - success
// status -1 - failed

class App extends Component {
  constructor(...args) {
    super(...args);

    this.state = {
      articles: [],
      status: 0,
      error: {},
    };
  }

  componentDidMount() {
    server
      .get("/search?tags=front_page")
      .then((res) => {
        const { hits } = res.data;
        this.setState({ articles: hits, status: 1 });
      })
      .catch((err) => {
        this.setState({ status: -1 });
      });
  }

  getSearch = (value, sort = "popularity", timestamp) => {
    this.setState({ status: 0 });
    let search = "search";
    let createdAt = "";
    const ts = Math.round(new Date().getTime() / 1000);
    if (sort === "date") {
      search = "search_by_date";
    }

    if (timestamp === "day") {
      const tsYesterday = ts - 24 * 3600;
      createdAt = `&numericFilters=created_at_i>${tsYesterday}`;
    } else if (timestamp === "week") {
      const tsYesterday = ts - 24 * 3600 * 7;
      createdAt = `&numericFilters=created_at_i>${tsYesterday}`;
    } else if (timestamp === "month") {
      const tsYesterday = ts - 24 * 3600 * 30;
      createdAt = `&numericFilters=created_at_i>${tsYesterday}`;
    }

    server
      .get(`/${search}?query=${value}&tags=story&${createdAt}`)
      .then((res) => {
        if (res.data.nbHits > 0) {
          const { hits } = res.data;

          this.setState({ articles: hits, status: 1, error: {} });
        } else {
          this.setState({
            error: { message: "No result found!" },
            status: -1,
            articles: [],
          });
        }
      })
      .catch((err) => {
        this.setState({
          error: { message: err.message },
          status: -1,
          articles: [],
        });
      });
  };

  rendered = () => {
    if (this.state.status === 0) {
      return <Loader />;
    } else if (this.state.status === -1) {
      return <Error error={this.state.error} />;
    } else {
      return <Results resultData={this.state.articles} />;
    }
  };

  render() {
    return (
      <>
        <Header getSearch={this.getSearch} />
        {this.rendered()}
      </>
    );
  }
}

export default App;
