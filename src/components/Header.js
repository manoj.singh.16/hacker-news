import React, { Component } from "react";
import config from "../config";
import Filter from "./Filter";

const debouncedSearch = (callback) => {
  let timer;

  return (value) => {
    clearTimeout(timer);
    timer = setTimeout(() => callback(value), config.debouncedTimer);
  };
};

class Header extends Component {
  state = {
    searchInput: "",
    sort: "popularity",
    timestamp: "all",
  };

  debounce = debouncedSearch((value) => {
    this.props.getSearch(value, this.state.sort, this.state.timestamp);
  });
  setInput = (e) => {
    this.setState({ searchInput: e.target.value });

    this.debounce(e.target.value);
  };

  changeFilters = (e) => {
    this.setState({ [e.target.name]: e.target.value });
  };

  componentDidUpdate(prevProp, prevState) {
    if (
      prevState.sort !== this.state.sort ||
      prevState.timestamp !== this.state.timestamp
    ) {
      this.props.getSearch(
        this.state.searchInput,
        this.state.sort,
        this.state.timestamp
      );
    }
  }

  render() {
    return (
      <>
        <nav>
          <div className="container">
            <h1 className="logo">
              <span>Search</span>
              <span>Hacker News</span>
            </h1>
            <div className="input-group">
              <i className="fas fa-search"></i>
              <input
                type="text"
                placeholder="Search stories by title, url or author"
                value={this.state.searchInput}
                onInput={this.setInput}
              />
            </div>
          </div>
        </nav>

        <Filter
          sort={this.state.sort}
          timestamp={this.state.timestamp}
          changeFilters={this.changeFilters}
        />
      </>
    );
  }
}

export default Header;
