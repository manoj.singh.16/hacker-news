import React, { Component } from "react";
import ResultItem from "./ResultItem";

export class Results extends Component {
  render() {
    const { resultData } = this.props;
    return (
      <div className="container result-wrapper">
        {resultData.length > 0 &&
          resultData.map((article, index) => (
            <ResultItem article={article} key={index} />
          ))}
      </div>
    );
  }
}

export default Results;
