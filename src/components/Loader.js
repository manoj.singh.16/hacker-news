import React, { Component } from "react";

export class Loader extends Component {
  render() {
    return (
      <div className="loader">
        <img src="/loader.gif" alt="loader" />
      </div>
    );
  }
}

export default Loader;
