import React, { Component } from "react";

export class Error extends Component {
  render() {
    return <div className="container error">{this.props.error.message}</div>;
  }
}

export default Error;
