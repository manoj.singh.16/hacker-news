import React, { Component } from "react";

export class ResultItem extends Component {
  render() {
    const { url, title, points, num_comments, author, created_at } =
      this.props.article;

    const date = new Date(created_at);
    return (
      <div className="result-item">
        <a className="heading" href={url}>
          <p>{title}</p>
          <span>{url ? `(${url})` : null}</span>
        </a>
        <div className="stats">
          <span>{points} points</span>
          <span>{author}</span>
          <span>{`${date.toDateString()} ${date.toLocaleTimeString()}`}</span>
          <span>{num_comments} comments</span>
        </div>
      </div>
    );
  }
}

export default ResultItem;
