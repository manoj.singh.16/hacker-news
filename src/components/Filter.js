import React, { Component } from "react";

export class Filter extends Component {
  render() {
    const { sort, timestamp, changeFilters } = this.props;
    return (
      <div className="filter-wrapper container">
        <span>Search</span>
        <span>by</span>
        <select name="sort" value={sort} onChange={changeFilters}>
          <option value="popularity">Popularity</option>
          <option value="date">Date</option>
        </select>
        <span>for</span>
        <select name="timestamp" value={timestamp} onChange={changeFilters}>
          <option value="all">All time</option>
          <option value="day">Past 24 hour</option>
          <option value="week">Past week</option>
          <option value="month">Past month</option>
        </select>
      </div>
    );
  }
}

export default Filter;
